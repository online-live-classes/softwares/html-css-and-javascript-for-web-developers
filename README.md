### Course Content

#### Day 1

* Understanding Markup Language
* All commonly used tags
* Basic syntaxes - Elements and Attributes 
* Headings, Paragraphs, Images, Hyperlinks, Lists, Tables
* Classes and id
* HTML Forms
* What's interesting in HTML5

#### Day 2

* CSS Syntaxes
* CSS Selectors
* Basic styling
* Advanced styling
* Add CSS to HTML - External, Internal & Inline

#### Day 3

* Create your own mini HTML site using HTML & CSS
* Familiarising CSS frameworks
* Convert your own mini HTML site with a CSS framework
* CSS Responsive

#### Day 4

* Javascript Syntaxes
* Javascript Variables & Operators
* Statements & Loops
* Functions
* Add JS to HTML
* Advanced features
* Familiarising JS frameworks & libraries

#### Day 5

* Mini project implementation